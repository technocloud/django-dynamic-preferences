from collections import namedtuple
from .settings import preferences_settings


def get_prefix(**kwargs):
    params_class = namedtuple("PrefixQuery", field_names=kwargs.keys())
    return preferences_settings.CACHE_PREFIX(params_class(**kwargs))
